const gulp = require('gulp');
const sass = require('gulp-sass');
const htmlValidator = require('gulp-w3c-html-validator');
const browserSync = require('browser-sync').create();

function sassCompile(endFolderPath) {
    gulp.src('./src/scss/style.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest(endFolderPath))
        .pipe(browserSync.reload({stream: true}));
}

gulp.task('server', gulp.series(
    function (done) {
        sassCompile('./src/css');
        done();
    },
    function (done) {
        browserSync.init({
            server: {
                baseDir: './src',
                open: "local",
            },
            notify: false
        });
        gulp.watch('./src/scss/**/*.scss', gulp.series('sass'));
        gulp.watch('./src/index.html').on('change', browserSync.reload);
        done()
    }
));

gulp.task('copy', function (done) {
    gulp.src('./src/*.html')
        .pipe(gulp.dest('./build'));
    done();
})

gulp.task('build', gulp.series(
    function (done) {
        sassCompile('./build/css');
        done();
    }, 'copy'
))

gulp.task('sass', function () {
    return sassCompile('./src/css');
});


gulp.task('validate-html', function () {
    return gulp.src('./src/index.html')
        .pipe(htmlValidator())
        .pipe(htmlValidator.reporter())
});




